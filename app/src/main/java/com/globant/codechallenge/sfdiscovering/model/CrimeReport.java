package com.globant.codechallenge.sfdiscovering.model;


import android.os.Parcel;
import android.os.Parcelable;

public class CrimeReport implements Parcelable {

    private String time;
    private String category;
    private District pddistrict;
    private String address;
    private String descript;
    private String dayOfWeek;
    private String resolution;
    private String date;
    private double y;
    private double x;
    private double incidntnum;

    public CrimeReport() {

    }

    private CrimeReport(Parcel in) {
        time = in.readString();
        category = in.readString();
        address = in.readString();
        descript = in.readString();
        dayOfWeek = in.readString();
        resolution = in.readString();
        date = in.readString();
        y = in.readDouble();
        x = in.readDouble();
        incidntnum = in.readDouble();
        pddistrict = in.readParcelable(District.class.getClassLoader());
    }

    public static final Creator<CrimeReport> CREATOR = new Creator<CrimeReport>() {
        @Override
        public CrimeReport createFromParcel(Parcel in) {
            return new CrimeReport(in);
        }

        @Override
        public CrimeReport[] newArray(int size) {
            return new CrimeReport[size];
        }
    };

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public District getPddistrict() {
        return pddistrict;
    }

    public void setPddistrict(District pddistrict) {
        this.pddistrict = pddistrict;
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescript() {
        return descript;
    }

    public void setDescript(String descript) {
        this.descript = descript;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getIncidntnum() {
        return incidntnum;
    }

    public void setIncidntnum(double incidntnum) {
        this.incidntnum = incidntnum;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(time);
        dest.writeString(category);
        dest.writeString(address);
        dest.writeString(descript);
        dest.writeString(dayOfWeek);
        dest.writeString(resolution);
        dest.writeString(date);
        dest.writeDouble(y);
        dest.writeDouble(x);
        dest.writeDouble(incidntnum);
        dest.writeParcelable(pddistrict, flags);
    }
}
