package com.globant.codechallenge.sfdiscovering.model;


import android.content.Context;

import com.android.volley.Request;
import com.globant.codechallenge.sfdiscovering.utilities.UtilDate;
import com.globant.codechallenge.sfdiscovering.net.helpers.HeaderHelper;
import com.globant.codechallenge.sfdiscovering.net.helpers.RequestHelper;
import com.globant.codechallenge.sfdiscovering.net.listeners.OnRequestDataListener;
import com.globant.codechallenge.sfdiscovering.net.mappers.DataNetMapper;
import com.globant.codechallenge.sfdiscovering.net.requests.SFDataAPIRequest;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DataSource {

    private static final String BEGIN_DATE = "?begin_date";
    private static final String END_DATE = "?end_date";
    public static final String PARAM_OFFSET = "?offset";
    public static final String SIZE_PAGE = "10";

    private static final String DOMAIN = "https://data.sfgov.org/resource/cuks-n6tp.json";

    private static final String CRIME_REPORTS_URL = DOMAIN + "?$where=date%20between%20%27"
            + BEGIN_DATE + "%27%20and%20%27" + END_DATE +
            "%27&$order=date,time%20DESC&$limit=" + SIZE_PAGE + "&$offset=" + PARAM_OFFSET;

    private static final String CRIME_REPORTS_BY_DISTRICT_URL =
            DOMAIN + "?$select=pddistrict,COUNT(*)&$group=pddistrict&$where=date%20between%20%27"
                    + BEGIN_DATE + "%27%20and%20%27" + END_DATE + "%27";


    private static DataSource dataSource = null;
    private Map<String, String> values = new HashMap<>();

    public static DataSource getInstance() {
        if (dataSource == null) {
            dataSource = new DataSource();
        }
        return dataSource;
    }


    private DataSource() {
    }

    public void getMoreDataFromSFApi(
            Context context,
            OnRequestDataListener<List<CrimeReport>> onRequestCrimeReportsListener) {
        Map<String, String> mapHeaders = new HashMap<>();
        HeaderHelper headerHelper = new HeaderHelper();
        headerHelper.putContentType(mapHeaders);
        DataNetMapper<List<CrimeReport>> dataNetMapper = new DataNetMapper<>(CrimeReport.class);
        SFDataAPIRequest<List<CrimeReport>> crimeReportsRequest = new SFDataAPIRequest<>(
                Request.Method.GET,
                createURL(CRIME_REPORTS_URL),
                onRequestCrimeReportsListener,
                dataNetMapper);
        crimeReportsRequest.setHeaders(mapHeaders);
        RequestHelper.getInstance(context).addRequest(crimeReportsRequest);
    }

    public void getCrimeReportByDistrct(
            Context context,
            OnRequestDataListener<List<District>> onCrimeReportsByDistrctListener) {
        Map<String, String> mapHeaders = new HashMap<>();
        HeaderHelper headerHelper = new HeaderHelper();
        headerHelper.putContentType(mapHeaders);
        DataNetMapper<List<District>> dataNetMapper = new DataNetMapper<>(District.class);
        SFDataAPIRequest<List<District>> crimeReportsRequest = new SFDataAPIRequest<>(
                Request.Method.GET,
                createURL(CRIME_REPORTS_BY_DISTRICT_URL),
                onCrimeReportsByDistrctListener,
                dataNetMapper);
        crimeReportsRequest.setHeaders(mapHeaders);
        RequestHelper.getInstance(context).addRequest(crimeReportsRequest);
    }

    public void setValues(Map<String, String> values) {
        if (values != null) {
            this.values = values;
        } else {
            this.values = new HashMap<>();
        }

        addValuesDate();
    }


    private String createURL(String url) {
        for (String key : values.keySet()) {
            url = url.replace(key, values.get(key));
        }
        return url;
    }

    private void addValuesDate() {
        long currentDate = System.currentTimeMillis();
        values.put(BEGIN_DATE, UtilDate.restTimeDate(Calendar.MONTH, -1, currentDate));
        values.put(END_DATE, UtilDate.getDateFormatted(currentDate));
    }
}
