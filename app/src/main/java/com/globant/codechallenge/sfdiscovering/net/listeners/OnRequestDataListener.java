package com.globant.codechallenge.sfdiscovering.net.listeners;


public interface OnRequestDataListener<T> {

    void onResponse(T object);

    void onError();
}
