package com.globant.codechallenge.sfdiscovering.utilities;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public final class UtilDate {

    private static final String PATTERN_DATE = "yyyy-MM-dd'T'00:00:00";
    public static final String TIME_ZONE = "GMT";

    public static String getDateFormatted(long date) {
        SimpleDateFormat sdf = new SimpleDateFormat(PATTERN_DATE);
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE));
        return sdf.format(new Date(date));
    }

    public static String restTimeDate(int type, int amount, long date) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(new Date(date));
        calendar.add(type, amount);
        return getDateFormatted(calendar.getTimeInMillis());
    }


}
