package com.globant.codechallenge.sfdiscovering.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.globant.codechallenge.sfdiscovering.R;
import com.globant.codechallenge.sfdiscovering.activities.MapActivity;
import com.globant.codechallenge.sfdiscovering.adapters.ReportCrimeAdapter;
import com.globant.codechallenge.sfdiscovering.model.CrimeReport;
import com.globant.codechallenge.sfdiscovering.model.DataSource;
import com.globant.codechallenge.sfdiscovering.model.District;
import com.globant.codechallenge.sfdiscovering.net.listeners.OnRequestDataListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ReportCrimeListFragment extends Fragment {

    public static final String KEY_SAVE_ESTATE_LIST_CRIME_REPORT =
            "KEY_SAVE_ESTATE_LIST_CRIME_REPORT";
    public static final String KEY_SAVE_ESTATE_CRIME_REPORT_BY_DISTRICT =
            "KEY_SAVE_ESTATE_CRIME_REPORT_BY_DISTRICT";
    private static final int INITIAL_OFFSET = 0;
    private List<District> crimeReportByDistrict;
    private List<CrimeReport> dataSet = new ArrayList<>();
    private ListView crimeReportListView;
    private ReportCrimeAdapter crimeReportAdapter;
    private boolean loading = false;

    private OnRequestDataListener<List<CrimeReport>> onRequestCrimeReportsListener =
            new OnRequestDataListener<List<CrimeReport>>() {
                @Override
                public void onResponse(List<CrimeReport> crimeReports) {
                    crimeReportAdapter.addAll(crimeReports);
                    loading = false;
                }

                @Override
                public void onError() {
                    Toast toast = Toast.makeText(
                            getActivity(),
                            getString(R.string.error_connection),
                            Toast.LENGTH_SHORT);
                    toast.show();
                }
            };

    private OnRequestDataListener<List<District>> onCrimeReportByDistrctListener =
            new OnRequestDataListener<List<District>>() {
                @Override
                public void onResponse(List<District> crimeReportsByDistrct) {
                    ReportCrimeListFragment.this.crimeReportByDistrict = crimeReportsByDistrct;
                    orderCrimeReportsByDistrct();
                    Map<String, String> values = new HashMap<>();
                    values.put(DataSource.PARAM_OFFSET, Integer.toString(INITIAL_OFFSET));
                    DataSource dataSource = DataSource.getInstance();
                    dataSource.setValues(values);
                    loading = true;
                    dataSource.getMoreDataFromSFApi(getActivity(), onRequestCrimeReportsListener);
                }

                @Override
                public void onError() {
                    Toast toast = Toast.makeText(
                            getActivity(),
                            getString(R.string.error_connection),
                            Toast.LENGTH_SHORT);
                    toast.show();
                }
            };


    public static Fragment newInstance() {
        return new ReportCrimeListFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_report_crime_list, container, false);
        setHasOptionsMenu(true);
        initView(view);
        setListeners();
        if (!restoreState(savedInstanceState)) {
            DataSource dataSource = DataSource.getInstance();
            dataSource.getCrimeReportByDistrct(getActivity(), onCrimeReportByDistrctListener);
        }
        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(
                KEY_SAVE_ESTATE_LIST_CRIME_REPORT,
                (ArrayList<CrimeReport>) dataSet);
        outState.putParcelableArrayList(
                KEY_SAVE_ESTATE_CRIME_REPORT_BY_DISTRICT,
                (ArrayList<District>) crimeReportByDistrict);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_report_crime_list, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_show_report_crime_map) {
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList(
                    MapActivity.KEY_REPORT_CRIME_LIST,
                    (ArrayList<CrimeReport>) dataSet);
            bundle.putParcelableArrayList(
                    MapActivity.KEY_REPORT_CRIME_BY_DISTRICT,
                    (ArrayList<District>) crimeReportByDistrict);
            Intent intent = new Intent(getActivity(), MapActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
        }
        
        return false;
    }


    private void initView(View view) {
        dataSet.clear();
        crimeReportListView = (ListView) view.findViewById(R.id.report_crime_list);
        crimeReportAdapter = new ReportCrimeAdapter(getActivity(), 0, dataSet);
        crimeReportListView.setAdapter(crimeReportAdapter);
    }

    private void setListeners() {
        crimeReportListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
                CrimeReport crimeReportSelected = crimeReportAdapter.getItem(position);
                bundle.putDouble(MapActivity.KEY_LATITUDE, crimeReportSelected.getY());
                bundle.putDouble(MapActivity.KEY_LONGITUDE, crimeReportSelected.getX());
                Intent intent = new Intent(getActivity(), MapActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        crimeReportListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view,
                                 int firstVisibleItem,
                                 int visibleItemCount,
                                 int totalItemCount) {
                boolean lastItem = (firstVisibleItem + visibleItemCount == totalItemCount);
                if (!loading && lastItem) {
                    loading = true;
                    Map<String, String> values = new HashMap<>();
                    int offset = crimeReportAdapter.getCount()
                            + Integer.valueOf(DataSource.SIZE_PAGE);
                    values.put(DataSource.PARAM_OFFSET, Integer.toString(offset));
                    DataSource dataSource = DataSource.getInstance();
                    dataSource.setValues(values);
                    dataSource.getMoreDataFromSFApi(getActivity(), onRequestCrimeReportsListener);
                }
            }
        });
    }

    private void orderCrimeReportsByDistrct() {
        Collections.sort(crimeReportByDistrict, new Comparator<District>() {
            @Override
            public int compare(District lhs, District rhs) {
                return Double.valueOf(lhs.getNumberOfReportsByMonth())
                        .compareTo(rhs.getNumberOfReportsByMonth());
            }
        });
    }

    private boolean restoreState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            crimeReportByDistrict = savedInstanceState
                    .getParcelableArrayList(KEY_SAVE_ESTATE_CRIME_REPORT_BY_DISTRICT);
            dataSet = savedInstanceState.getParcelableArrayList(KEY_SAVE_ESTATE_LIST_CRIME_REPORT);
            if (dataSet != null) {
                crimeReportAdapter.addAll(dataSet);
            }
            return true;
        }
        return false;
    }
}
