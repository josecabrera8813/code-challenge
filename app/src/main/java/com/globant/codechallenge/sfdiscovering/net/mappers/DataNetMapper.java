package com.globant.codechallenge.sfdiscovering.net.mappers;

import com.globant.codechallenge.sfdiscovering.model.CrimeReport;
import com.globant.codechallenge.sfdiscovering.model.District;

import org.json.JSONException;

public class DataNetMapper<T> {

    private Class mTypeClass;


    public DataNetMapper(Class typeClass) {
        this.mTypeClass = typeClass;
    }

    public T mapper(String dataToMapper) throws JSONException {
        return jsonMapper(dataToMapper);
    }

    private T jsonMapper(String dataToMapper) throws JSONException {
        if (mTypeClass.equals(CrimeReport.class)) {
            return (T) CrimeReportMapper.parseJsonToListCrimeReport(dataToMapper);
        } else if (mTypeClass.equals(District.class)) {
            return (T) DistrictMapper.parseJsonToListCrimeReport(dataToMapper);
        } else {
            throw new JSONException("Can not parser the data : " + dataToMapper);
        }
    }
}
