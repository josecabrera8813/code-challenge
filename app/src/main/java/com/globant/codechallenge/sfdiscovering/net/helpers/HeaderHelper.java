package com.globant.codechallenge.sfdiscovering.net.helpers;


import java.util.Map;

public class HeaderHelper {

    public static final String CONTENT_TYPE = "Content-Type";
    public static final String CONTENT_TYPE_VALUE = "application/json";

    public void putContentType(Map<String, String> hashMap) {
        hashMap.put(CONTENT_TYPE, CONTENT_TYPE_VALUE);
    }
}
