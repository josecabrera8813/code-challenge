package com.globant.codechallenge.sfdiscovering.net.requests;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.globant.codechallenge.sfdiscovering.net.listeners.OnRequestDataListener;
import com.globant.codechallenge.sfdiscovering.net.mappers.DataNetMapper;

import org.json.JSONException;

import java.util.Map;

public class SFDataAPIRequest<T> extends Request<T> {

    private DataNetMapper<T> dataNetMapper;
    private OnRequestDataListener<T> listener;
    private Map<String, String> headers;

    public SFDataAPIRequest(int method, String url,
                            OnRequestDataListener<T> listener, DataNetMapper<T> dataNetMapper) {
        super(method, url, createErrorListener(listener));
        this.listener = listener;
        this.dataNetMapper = dataNetMapper;
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            return Response.success(parseResponse(response), HttpHeaderParser.parseCacheHeaders(response));
        } catch (JSONException e) {
            return Response.error(new VolleyError());
        }
    }


    @Override
    protected void deliverResponse(T response) {
        listener.onResponse(response);
    }


    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return headers;
    }

    private T parseResponse(NetworkResponse response) throws JSONException {
        return dataNetMapper.mapper(new String(response.data));
    }

    private static Response.ErrorListener createErrorListener(
            final OnRequestDataListener listener) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onError();
            }
        };
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }
}
