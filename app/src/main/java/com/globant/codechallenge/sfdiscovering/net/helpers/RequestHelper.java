package com.globant.codechallenge.sfdiscovering.net.helpers;


import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.globant.codechallenge.sfdiscovering.net.requests.SFDataAPIRequest;

public class RequestHelper {

    private static RequestHelper requestHelper;
    private RequestQueue requestQueue;

    public static RequestHelper getInstance(Context context) {
        if (requestHelper == null) {
            requestHelper = new RequestHelper(context);
            return requestHelper;
        }
        return requestHelper;
    }

    private RequestHelper(Context context) {
        requestQueue = Volley.newRequestQueue(context);
    }

    public void addRequest(SFDataAPIRequest sfDataAPIRequest) {
        requestQueue.add(sfDataAPIRequest);
    }
}
