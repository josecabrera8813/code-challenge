package com.globant.codechallenge.sfdiscovering.activities;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;

import com.globant.codechallenge.sfdiscovering.R;
import com.globant.codechallenge.sfdiscovering.fragments.ReportCrimeListFragment;


public class ReportedCrimeListActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragment(ReportCrimeListFragment.newInstance(),
                ReportedCrimeListActivity.class.getSimpleName());
    }

    @Override
    protected void setUpActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.app_name));
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.tool_bar_color));
        setSupportActionBar(toolbar);
    }
}
