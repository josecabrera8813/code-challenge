package com.globant.codechallenge.sfdiscovering.activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.globant.codechallenge.sfdiscovering.R;


public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_with_single_fragment);
        setUpActionBar();
    }

    protected void setFragment(Fragment fragment, String fragmentTag) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment fragmentInMemory = fm.findFragmentByTag(fragmentTag);
        if (fragmentInMemory != null) {
            ft.replace(R.id.general_fragment, fragmentInMemory, fragmentTag);
            ft.commit();
        } else {
            ft.replace(R.id.general_fragment, fragment, fragmentTag);
            ft.commit();
        }

    }

    protected abstract void setUpActionBar();
}
