package com.globant.codechallenge.sfdiscovering.model;

import android.os.Parcel;
import android.os.Parcelable;

public class District implements Parcelable {
    private String name;
    private double numberOfReportsByMonth;
    private int color;

    public District() {
    }

    private District(Parcel in) {
        name = in.readString();
        numberOfReportsByMonth = in.readDouble();
        color = in.readInt();
    }

    public static final Creator<District> CREATOR = new Creator<District>() {
        @Override
        public District createFromParcel(Parcel in) {
            return new District(in);
        }

        @Override
        public District[] newArray(int size) {
            return new District[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getNumberOfReportsByMonth() {
        return numberOfReportsByMonth;
    }

    public void setNumberOfReportsByMonth(double numberOfReportsByMonth) {
        this.numberOfReportsByMonth = numberOfReportsByMonth;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeDouble(numberOfReportsByMonth);
        dest.writeInt(color);
    }
}
