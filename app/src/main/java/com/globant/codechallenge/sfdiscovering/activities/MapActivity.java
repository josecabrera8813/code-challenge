package com.globant.codechallenge.sfdiscovering.activities;


import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;

import com.globant.codechallenge.sfdiscovering.R;
import com.globant.codechallenge.sfdiscovering.model.District;
import com.globant.codechallenge.sfdiscovering.model.CrimeReport;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MapActivity extends BaseActivity implements OnMapReadyCallback {

    public static final String KEY_LATITUDE = "KEY_LATITUDE";
    public static final String KEY_LONGITUDE = "KEY_LONGITUDE";
    public static final String KEY_REPORT_CRIME_LIST = "KEY_REPORT_CRIME_LIST";
    public static final String KEY_REPORT_CRIME_BY_DISTRICT = "KEY_REPORT_CRIME_BY_DISTRICT";
    private static final int ZOOM = 13;

    private static final int[] COLOR_KEY = new int[]{
            R.color.level_one_crime,
            R.color.level_two_crime,
            R.color.level_three_crime,
            R.color.level_four_crime,
            R.color.level_five_crime,
            R.color.level_six_crime,
            R.color.level_seven_crime,
            R.color.level_eight_crime
    };

    private double latitude;
    private double longitude;
    private List<CrimeReport> dataSet;
    private Map<String, District> mapDistrictColor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        setUpActionBar();
        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void onMapReady(GoogleMap map) {


        if (readCrimePositionExtras()) {
            LatLng reportCrimePosition = new LatLng(latitude, longitude);
            map.setMyLocationEnabled(true);
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(reportCrimePosition, ZOOM));
            map.addMarker(new MarkerOptions()
                    .position(reportCrimePosition));
        }

        if (readReportsCrimebyDistrictExtras()) {
            for (CrimeReport crimeReport : dataSet) {
                LatLng reportCrimePosition = new LatLng(crimeReport.getY(), crimeReport.getX());
                map.setMyLocationEnabled(true);
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(reportCrimePosition, ZOOM));
                map.addMarker(new MarkerOptions()
                        .position(reportCrimePosition)
                        .icon(BitmapDescriptorFactory.fromBitmap(generateIcon(crimeReport))));

            }
        }
    }

    private Bitmap generateIcon(CrimeReport crimeReport) {
        int color = ContextCompat.getColor(
                this, mapDistrictColor.get(crimeReport.getPddistrict().getName()).getColor());
        IconGenerator iconGenerator = new IconGenerator(this);
        iconGenerator.setColor(color);
        return iconGenerator.makeIcon();
    }


    private boolean readReportsCrimebyDistrictExtras() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey(KEY_REPORT_CRIME_LIST)
                && bundle.containsKey(KEY_REPORT_CRIME_BY_DISTRICT)) {
            dataSet = bundle.getParcelableArrayList(KEY_REPORT_CRIME_LIST);
            List<District> reportsCrimeByDistrict =
                    bundle.getParcelableArrayList(KEY_REPORT_CRIME_BY_DISTRICT);
            buildMapDistrictColor(reportsCrimeByDistrict);
            return true;
        }
        return false;
    }

    private void buildMapDistrictColor(List<District> reportsCrimeByDistrict) {
        int i = 0;
        mapDistrictColor = new HashMap<>();

        for (; i < COLOR_KEY.length; i++) {
            District district = reportsCrimeByDistrict.get(i);
            district.setColor(COLOR_KEY[i]);
            mapDistrictColor.put(district.getName(), district);
        }

        for (; i < reportsCrimeByDistrict.size(); i++) {
            District district = reportsCrimeByDistrict.get(i);
            district.setColor(COLOR_KEY[COLOR_KEY.length - 1]);
            mapDistrictColor.put(district.getName(), district);
        }

    }

    private boolean readCrimePositionExtras() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey(KEY_LATITUDE)
                && bundle.containsKey(KEY_LONGITUDE)) {
            latitude = bundle.getDouble(KEY_LATITUDE);
            longitude = bundle.getDouble(KEY_LONGITUDE);
            return true;
        }
        return false;
    }

    @Override
    protected void setUpActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.map_section));
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.tool_bar_color));
        setSupportActionBar(toolbar);
    }
}
