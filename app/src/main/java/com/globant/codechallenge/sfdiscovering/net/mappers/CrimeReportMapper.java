package com.globant.codechallenge.sfdiscovering.net.mappers;


import com.globant.codechallenge.sfdiscovering.model.CrimeReport;
import com.globant.codechallenge.sfdiscovering.model.District;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CrimeReportMapper {

    private static final String KEY_JSON_ADDRESS = "address";
    private static final String KEY_JSON_CATEGORY = "category";
    private static final String KEY_JSON_DATE = "date";
    private static final String KEY_JSON_DAY_OF_WEEK = "dayofweek";
    private static final String KEY_JSON_DESCRIPT = "descript";
    private static final String KEY_JSON_INCIDNTNUM = "incidntnum";
    private static final String KEY_JSON_RESOLUTION = "resolution";
    private static final String KEY_JSON_TIME = "time";
    private static final String KEY_JSON_X = "x";
    private static final String KEY_JSON_Y = "y";
    private static final String KEY_PDDISTRICT = "pddistrict";

    public static List<CrimeReport> parseJsonToListCrimeReport(String dataToMapper)
            throws JSONException {
        List<CrimeReport> crimeReports = new ArrayList<>();
        JSONArray jsonCrimeReports = new JSONArray(dataToMapper);
        for (int i = 0; i < jsonCrimeReports.length(); i++) {
            JSONObject jsonCrimeReport = jsonCrimeReports.optJSONObject(i);
            crimeReports.add(parseJsonToCrimeReport(jsonCrimeReport));
        }
        return crimeReports;
    }

    public static CrimeReport parseJsonToCrimeReport(JSONObject jsonCrimeReport)
            throws JSONException {
        CrimeReport crimeReport = new CrimeReport();
        crimeReport.setAddress(jsonCrimeReport.optString(KEY_JSON_ADDRESS));
        crimeReport.setCategory(jsonCrimeReport.optString(KEY_JSON_CATEGORY));
        crimeReport.setDate(jsonCrimeReport.optString(KEY_JSON_DATE));
        crimeReport.setDayOfWeek(jsonCrimeReport.optString(KEY_JSON_DAY_OF_WEEK));
        crimeReport.setDescript(jsonCrimeReport.optString(KEY_JSON_DESCRIPT));
        crimeReport.setIncidntnum(jsonCrimeReport.optDouble(KEY_JSON_INCIDNTNUM));
        crimeReport.setPddistrict(parseJsonToDistrictCrime(jsonCrimeReport));
        crimeReport.setResolution(jsonCrimeReport.getString(KEY_JSON_RESOLUTION));
        crimeReport.setTime(jsonCrimeReport.getString(KEY_JSON_TIME));
        crimeReport.setX(jsonCrimeReport.getDouble(KEY_JSON_X));
        crimeReport.setY(jsonCrimeReport.getDouble(KEY_JSON_Y));
        return crimeReport;
    }

    public static District parseJsonToDistrictCrime(JSONObject jsonCrimeReport)
            throws JSONException {
        District district = new District();
        district.setName(jsonCrimeReport.getString(KEY_PDDISTRICT));
        return district;

    }
}
