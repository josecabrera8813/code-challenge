package com.globant.codechallenge.sfdiscovering.net.mappers;


import com.globant.codechallenge.sfdiscovering.model.District;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DistrictMapper {

    public static final String KEY_JSON_PD_DISTRICT = "pddistrict";
    public static final String KEY_JSON_COUNT = "COUNT";

    public static List<District> parseJsonToListCrimeReport(String dataToMapper)
            throws JSONException {
        List<District> districts = new ArrayList<>();
        JSONArray jsonDistricts = new JSONArray(dataToMapper);
        for (int i = 0; i < jsonDistricts.length(); i++) {
            JSONObject jsonDistrict = jsonDistricts.optJSONObject(i);
            districts.add(parseJsonToDistrict(jsonDistrict));
        }
        return districts;
    }

    private static District parseJsonToDistrict(JSONObject jsonDistrict) throws JSONException {
        District district = new District();
        district.setName(jsonDistrict.optString(KEY_JSON_PD_DISTRICT));
        district.setNumberOfReportsByMonth(jsonDistrict.getDouble(KEY_JSON_COUNT));
        return district;
    }
}
