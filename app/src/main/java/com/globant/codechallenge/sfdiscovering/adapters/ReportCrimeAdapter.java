package com.globant.codechallenge.sfdiscovering.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.globant.codechallenge.sfdiscovering.R;
import com.globant.codechallenge.sfdiscovering.model.CrimeReport;

import java.util.List;


public class ReportCrimeAdapter extends ArrayAdapter<CrimeReport> {

    public ReportCrimeAdapter(Context context, int resource, List<CrimeReport> dataSet) {
        super(context, resource, dataSet);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;

        if (view == null) {
            view = LayoutInflater.from(getContext())
                    .inflate(R.layout.report_crime_item, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        fillHolder(holder, position);

        return view;
    }

    private void fillHolder(ViewHolder holder, int position) {
        CrimeReport crimeReport = getItem(position);
        if (crimeReport != null) {
            holder.district.setText(crimeReport.getPddistrict().getName());
            holder.category.setText(crimeReport.getCategory());
            holder.address.setText(crimeReport.getAddress());
            holder.date.setText(crimeReport.getDate());
        }
    }

    private static class ViewHolder {
        public TextView district;
        public TextView category;
        public TextView address;
        public TextView date;

        ViewHolder(View view) {
            district = (TextView) view.findViewById(R.id.pddistrict);
            category = (TextView) view.findViewById(R.id.category);
            address = (TextView) view.findViewById(R.id.addres);
            date = (TextView) view.findViewById(R.id.date);
        }
    }
}
